﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movementjump : MonoBehaviour
{

    private float move;
    private Vector3 movement;
    private Rigidbody2D rigidbody;
    private int ok = 1;
    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "pamant")
            ok = 1;
        if (col.gameObject.tag == "wall")
        {
            GameObject.FindGameObjectWithTag("LVLMAN").GetComponent<LevelManager>().Lose();
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("w") && ok==1)
        {
            if (transform.position.y > 0.2f)
                ok = 0;
            rigidbody.AddForce(new Vector2(0f,35f));
        }
        if (Input.GetKeyUp("w"))
        {
            ok = 0;
        }
        
    }
}
