﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reference_Rotate : MonoBehaviour
{
    private Vector3 MousePos; // pozitia curenta a mouse.ului
    private Vector2 look; // pozitia curenta a mouse.ului PE ECRAN
    private float angle; // Unghiul la care trebuie sa ne rotim pe axa Z
    private Quaternion rotation; //Rotatia necesare
    private float speed = 5f; //Viteza de rotaties

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MousePos = Input.mousePosition;
        look = Camera.main.ScreenToWorldPoint(MousePos) - transform.position;
        angle = Mathf.Atan2(look.y, look.x) * Mathf.Rad2Deg;
        rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        rotation *= Quaternion.Euler(0f, 0f, 90f);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speed * Time.deltaTime);
    }
}
