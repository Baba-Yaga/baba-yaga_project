﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class MinigameContainer
{
    public static MinigameContainer_SO so { get; set; }
    public static int ok = 1;

    public static void GenerateMinigames()
    {
        if (ok == 1)
        {
            ok = 0; Minigame m;


            //Debug.Log(so.Actions.Count);
            for (int i = 0; i < 2; i++)
            {
                m = new Minigame(1);
                m.Text = File.ReadAllText("Assets/Resources/Main/Databases/Lockpicking/Lockpicking.txt");
                m.WinText = File.ReadAllText("Assets/Resources/Main/Databases/Lockpicking/Win.txt");
                m.LoseText = File.ReadAllText("Assets/Resources/Main/Databases/Lockpicking/Lose.txt");
                so.Actions.Add(m);
            }
            //Debug.Log(so.Actions.Count);

            for (int i = 0; i < 2; i++)
            {
                m = new Minigame(2);
                m.Text = File.ReadAllText("Assets/Resources/Main/Databases/Maze/Maze.txt");
                m.WinText = File.ReadAllText("Assets/Resources/Main/Databases/Maze/Win.txt");
                m.LoseText = File.ReadAllText("Assets/Resources/Main/Databases/Maze/Lose.txt");
                so.Actions.Add(m);
            }
            //Debug.Log(so.Actions.Count);

            for (int i = 0; i < 2; i++)
            {
                m = new Minigame(3);
                m.Text = File.ReadAllText("Assets/Resources/Main/Databases/Catch/Catch.txt");
                m.WinText = File.ReadAllText("Assets/Resources/Main/Databases/Catch/Win.txt");
                m.LoseText = File.ReadAllText("Assets/Resources/Main/Databases/Catch/Lose.txt");
                so.Actions.Add(m);
            }
            //Debug.Log(so.Actions.Count);

            for (int i = 0; i < 2; i++)
            {
                m = new Minigame(4);
                m.Text = File.ReadAllText("Assets/Resources/Main/Databases/Jump/Jump.txt");
                m.WinText = File.ReadAllText("Assets/Resources/Main/Databases/Jump/Win.txt");
                m.LoseText = File.ReadAllText("Assets/Resources/Main/Databases/Jump/Lose.txt");
                so.Actions.Add(m);
            }
            //Debug.Log(so.Actions.Count);

            for (int i = 0; i < 2; i++)
            {
                m = new Minigame(5);
                m.Text = File.ReadAllText("Assets/Resources/Main/Databases/Shoot/Shoot.txt");
                m.WinText = File.ReadAllText("Assets/Resources/Main/Databases/Shoot/Win.txt");
                m.LoseText = File.ReadAllText("Assets/Resources/Main/Databases/Shoot/Lose.txt");
                so.Actions.Add(m);
            }
            //Debug.Log(so.Actions.Count);

            /*m = new Minigame(1);
            m.Text = File.ReadAllText("Assets/Resources/Main/Databases/Hit/Hit.txt");
            m.WinText = File.ReadAllText("Assets/Resources/Main/Databases/Hit/Win.txt");
            m.LoseText = File.ReadAllText("Assets/Resources/Main/Databases/Hit/Lose.txt");
            so.Actions.Add(m);*/

            /*for(int i=0; i< so.Actions.Count; i++)
            {
                Debug.Log(so.Actions[i].Text);
            }*/
        }

    }
}
