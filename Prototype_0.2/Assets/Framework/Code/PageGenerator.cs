﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public static class PageGenerator
{
    public static ButtonPage rightPage;
    public static TextPage leftPage;
    public static int ok1 = 1, ok2 = 1;

    public static void GenerateLeftPage(string nextPageText)
    {
        if(ok1==1)
        {
            leftPage = GameObject.FindWithTag("MaImpusc").GetComponent<BookData>().LeftPage;
            string a = "Going forward, I was presented with the following choices..."; string b = nextPageText;
            string x = b + "\n" + "\n" + a; //Debug.Log(x);
            leftPage.Paragraph = ParagraphMaker.CreateParagraph(x);
        }
        ok1 = 1-ok1;
    }

    public static void GenerateLeftPageFinal()
    {
        if (ok1 == 1)
        {
            leftPage = GameObject.FindWithTag("MaImpusc").GetComponent<BookData>().LeftPage;
            string a = "They got me now..." + "\n" +"They'll have my skin for what I've done";
            leftPage.Paragraph = ParagraphMaker.CreateParagraph(a);
        }
        ok1 = 1 - ok1;
    }

    public static void GenerateRightPage()
    {
        if(ok2==1 && MinigameContainer.so.Actions.Count>0)
        {
            rightPage = GameObject.FindWithTag("MaImpusc").GetComponent<BookData>().RightPage;
            Button b = rightPage.Action1.GetComponent<Button>();
            if (b.Clicked == true)
                GenerateBigButton(rightPage.Action1);
            b = rightPage.Action2.GetComponent<Button>();
            if (b.Clicked == true)
                GenerateBigButton(rightPage.Action2);
            b = rightPage.Action3.GetComponent<Button>();
            if (b.Clicked == true)
                GenerateBigButton(rightPage.Action3);
        }
        else if (ok2==1 && MinigameContainer.so.Actions.Count==0)
        {
            rightPage = GameObject.FindWithTag("MaImpusc").GetComponent<BookData>().RightPage;
            Button b = rightPage.Action1.GetComponent<Button>();
            if (b.Clicked == true)
                FreeBigButton(rightPage.Action1);
            b = rightPage.Action2.GetComponent<Button>();
            if (b.Clicked == true)
                FreeBigButton(rightPage.Action2);
            b = rightPage.Action3.GetComponent<Button>();
            if (b.Clicked == true)
                FreeBigButton(rightPage.Action3);
        }
        
        ok2 = 1-ok2;
    }

    public static void GenerateBigButton(GameObject button)
    {
        int index = Random.Range(0, MinigameContainer.so.Actions.Count); Debug.Log(index + " " + MinigameContainer.so.Actions.Count);
        button.GetComponent<Button>().ButtonAction = MinigameContainer.so.Actions[index];
        button.GetComponent<Button>().Clicked = false;
        button.GetComponentInChildren<TextMeshPro>().text = MinigameContainer.so.Actions[index].Text; 
        MinigameContainer.so.Actions.RemoveAt(index);
    }
    public static void FreeBigButton(GameObject button)
    {
        button.GetComponent<Button>().Clicked = false;
        button.GetComponentInChildren<TextMeshPro>().text = null;
        button.GetComponent<BoxCollider2D>().enabled = false;
    }
}
