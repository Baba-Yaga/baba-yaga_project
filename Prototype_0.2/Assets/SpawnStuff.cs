﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStuff : MonoBehaviour
{
    private GameObject baiatRau;
    private GameObject baiatBun;
    private float timer = 0f;
    private float horSize;
    private float verSize;

    // Start is called before the first frame update
    void Start()
    {
        verSize = Camera.main.orthographicSize;
        horSize = Camera.main.orthographicSize * Screen.width / Screen.height;
        horSize = Mathf.Floor(horSize);
        baiatRau = Resources.Load("Main/OtherGameobjects/Prefabs/CatchGame/badThing") as GameObject;
        baiatBun = Resources.Load("Main/OtherGameobjects/Prefabs/CatchGame/goodThing") as GameObject;
        timer = Random.Range(0f,1f);
       /// print(horSize);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
       /// print(timer);
        if(timer<0.0f)
        {
            float rand = Random.value;
            
            if (rand <= 0.7f)
            {
                Instantiate(baiatBun, new Vector3(Random.Range(-horSize, horSize), verSize + 0.5f, 0f), Quaternion.identity);
            }
            else
            {
                Instantiate(baiatRau, new Vector3(Random.Range(-horSize, horSize), verSize + 0.5f, 0f), Quaternion.identity);
            }
            timer = Random.Range(0f, 1f);
        }
        
    }
}
