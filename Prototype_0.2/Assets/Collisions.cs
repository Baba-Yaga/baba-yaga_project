﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "target")
        {
            GameObject.FindGameObjectWithTag("LVLMAN").GetComponent<LevelManager>().Win();
        }
        else if (col.gameObject.tag == "wall")
        {
            shoot.tries--;
            
            if (shoot.tries==0)
            {
                GameObject.FindGameObjectWithTag("LVLMAN").GetComponent<LevelManager>().Lose();
            }
        }
        shoot.ok = 1;
        Destroy(this.gameObject);
    }
}
