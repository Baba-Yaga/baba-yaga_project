﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shoot : MonoBehaviour
{
    private GameObject projectile_; // Pt. instantiate
    private GameObject projectile; // pt dupa
    private Vector3 MousePos; // pozitia curenta a mouse.ului
    public Camera camera;
    public static int tries;
    /*private Vector2 look; // pozitia curenta a mouse.ului PE ECRAN
    private float angle; // Unghiul la care trebuie sa ne rotim pe axa Z
    private Quaternion rotation; //Rotatia necesare
    private float speed = 5f; //Viteza de rotaties*/


    public LineRenderer line; //efectiv linia
    public int segmentCount = 20; //nr de seg calculate, cu cat mai mult cu atat mai precis
    public float segmentScale = 1f; // lung unui seg
    private float FireStrenght =  1000f;
    private LineRenderer linerenderer;
    public static int ok = 1;
    public Transform reference_Transform;
    public Text text;

    void Start()
    {
        line.SetColors(Color.red, Color.red);
        line.SetVertexCount(segmentCount);
        projectile_ = Resources.Load("Main/OtherGameobjects/Prefabs/Shoot/Ball") as GameObject;
        tries = 2;
        ///linerenderer = line.GetComponent<LineRenderer>();
        ///linerenderer.enabled = false;
    }


    public void CameraFollow(Transform target)
    {
        camera.transform.position = target.position + new Vector3(8f, 3.05f, -10f);
    }

    void FixedUpdate()
    {
        if (ok == 1)
            CameraFollow(transform);
        else
            CameraFollow(projectile.transform);
        MousePos = Input.mousePosition;
        /*look = Camera.main.ScreenToWorldPoint(MousePos) - transform.position;
        angle = Mathf.Atan2(look.y, look.x) * Mathf.Rad2Deg;
        rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        rotation *= Quaternion.Euler(0f, 0f, 90f);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speed * Time.deltaTime);*/


        Vector3[] segments = new Vector3[segmentCount];
        if (Input.GetMouseButtonDown(0) && ok == 1)
        {
            
            Vector2 ClickLoc = Camera.main.ScreenToWorldPoint(MousePos); // poz mouse
            reference_Transform.position = ClickLoc;
            print(reference_Transform.position);
        }
        if (Input.GetMouseButton(0) && ok == 1)
        {
            ///linerenderer.enabled = true;
            segments[0] = transform.position + new Vector3(1f,1f,0f);
            Vector2 locMouse = Camera.main.ScreenToWorldPoint(MousePos) - reference_Transform.position;
            float abscisa = Mathf.Abs(locMouse.x);
            float ordonata = Mathf.Abs(locMouse.y);
            FireStrenght = Mathf.Pow(abscisa,2) + Mathf.Pow(ordonata,2);
            FireStrenght = Mathf.Sqrt(FireStrenght);
            FireStrenght *= 100f;
            Mathf.Clamp(FireStrenght, 0, 500);
            Vector3 segVelocity = reference_Transform.up * FireStrenght * Time.deltaTime;


            for (int i = 1; i < segmentCount; i++)
            {
                float segTime = (segVelocity.sqrMagnitude != 0) ? segmentScale / segVelocity.magnitude : 0; // timpul in care se parcurge un segment

                segVelocity = segVelocity + Physics.gravity * segTime; //adaug efectul gravitatii


                segments[i] = segments[i - 1] + segVelocity * segTime;
            }
            for (int i = 0; i < segmentCount; i++)
                line.SetPosition(i, segments[i]);
        }
        if(Input.GetMouseButtonUp(0) && ok==1)
        {
            ok = 0;
            Vector3 inst_vector = new Vector3(transform.position.x + 1f, transform.position.y + 1f, 0f);
            projectile = (GameObject)Instantiate(projectile_, inst_vector, reference_Transform.rotation);
            projectile.GetComponent<Rigidbody2D>().AddForce(projectile.transform.up * FireStrenght);
        }

        
        
    }
}
