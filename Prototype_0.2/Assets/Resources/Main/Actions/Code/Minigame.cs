﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Minigame : Action
{
    public string WinText { get; set; }
    public string LoseText { get; set; }

    private int lifes = 2;

    public Minigame() { }

    public Minigame(int SceneIndex)
    {
        this.SceneIndex = SceneIndex;
    }

    public override void DoAction()
    {
        /*Animator a = GameObject.FindWithTag("MaImpusc").GetComponent<Animator>();
        a.SetTrigger("BookSceneExit");
        while(a.)*/
        LoadLevel(SceneIndex);
    }

    public override void EndOfAction()
    {
        //se proceseaza rezultatele actiunii 
        //Debug.Log("M_AM INTORS");
        switch (OutcomeContainer.Win)
        {
            case true: PageGenerator.GenerateLeftPage(WinText); break;
            case false: PageGenerator.GenerateLeftPage(LoseText + "\n" + "Now they are " + --lifes + " step away from me"); break;

        }
        PageGenerator.GenerateRightPage();
    }
}
