﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class BookGenerator : MonoBehaviour
{
    public GameObject RightPage;
    public GameObject LeftPage;
    protected ButtonPage buttonPage;
    protected TextPage textPage;

    private void Start()
    {
        buttonPage = RightPage.GetComponent<ButtonPage>();
        textPage = LeftPage.GetComponent<TextPage>();
    }
}
