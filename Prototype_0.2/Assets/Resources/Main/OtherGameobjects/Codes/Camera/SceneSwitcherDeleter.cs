﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSwitcherDeleter : MonoBehaviour
{
    public GameObject Book;

    private void Start()
    {
        Book = GameObject.FindWithTag("MaImpusc");
        Book.GetComponent<Animator>().SetTrigger("BookSceneStart");
        MinigameContainer.so = Resources.Load<MinigameContainer_SO>("Main/MinigameContainer");
        MinigameContainer.GenerateMinigames();
        PageGenerator.GenerateLeftPage("My name is Panda..." + "\n" +
                                        "and this is my magic diary." + "\n" +
                                        "You are going to help me escape" + "\n" +
                                        " from the people that want to torture me." + "\n\n\n" +
                                        "I have stolen from the rich and evil bears" + "\n" +
                                        "to give to the poor and unhappy ones." + "\n" +
                                        "Now I am being followed and I can't afford to make many mistakes." + "\n" +
                                        "So let's get started");
        PageGenerator.GenerateRightPage();
    }

    private void LateUpdate()
    {
        if(staticadegeaba.ok)
        {
            /*float t = 0;
            while (GameObject.Find("Book") == null) { t += Time.deltaTime; };
            Destroy(GameObject.Find("Book"));
            Book = GameObject.Find("BOOK");
            Book.name = "Book"; print(t);*/
            GameObject[] x = new GameObject[1000];
            x = GameObject.FindGameObjectsWithTag("MaImpusc"); print(x.Length);
            Book = x[0];
            Destroy(x[1]);
            
            staticadegeaba.ok = false;

            //proceseaza datele de la intoarcere
            Book.GetComponentInChildren<ButtonPage>().selectedAction.EndOfAction();
        }
    }
}
