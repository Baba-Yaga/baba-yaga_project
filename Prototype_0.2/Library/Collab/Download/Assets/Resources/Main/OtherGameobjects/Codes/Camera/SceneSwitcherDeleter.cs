﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSwitcherDeleter : MonoBehaviour
{
    public GameObject Book;

    private void Start()
    {
        Book = GameObject.FindWithTag("MaImpusc");
        MinigameContainer.so = Resources.Load<MinigameContainer_SO>("Main/MinigameContainer");
        MinigameContainer.GenerateMinigames();
        PageGenerator.GenerateLeftPage();
        PageGenerator.GenerateRightPage();
    }

    private void Update()
    {
        if(staticadegeaba.ok)
        {
            GameObject[] x = new GameObject[2];
            x = GameObject.FindGameObjectsWithTag("MaImpusc");
            Destroy(x[1]);
            Book = x[0];
            staticadegeaba.ok = false;

            //proceseaza datele de la intoarcere
            Book.GetComponentInChildren<ButtonPage>().selectedAction.EndOfAction();
        }
    }
}
