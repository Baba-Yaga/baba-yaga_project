﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private float moveHorizontal;
    private float moveVertical;
    private Vector3 movement;
    private Transform transform;
    float speed = 14f;
    bool move = true;
    // Start is called before the first frame update
    void Start()
    {
        transform = GetComponent<Transform>();
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "wall")
            move = false;
    }

    void OnCollisionExit2D(Collision2D col)
    {
         move = true;
    }

    void OnCollisionStay2D(Collision2D col)
    {
        move = true;
    }
    // Update is called once per frame
    void Update()
    {
       /// print(move);
        if (move == true)
        {
            moveHorizontal = Input.GetAxisRaw("Horizontal");
            moveVertical = Input.GetAxisRaw("Vertical");

            movement = new Vector3(moveHorizontal, moveVertical, 0.0f);

            transform.position += movement / speed;
        }
    }
}
