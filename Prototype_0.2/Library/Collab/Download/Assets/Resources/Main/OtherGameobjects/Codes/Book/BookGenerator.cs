﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookGenerator : MonoBehaviour
{
    public GameObject RightPage;
    public GameObject LeftPage;
    protected ButtonPage buttonPage;
    protected TextPage textPage;

    private void Start()
    {
        buttonPage = RightPage.GetComponent<ButtonPage>();
        textPage = LeftPage.GetComponent<TextPage>();

        GenerateMinigames();
    }

    public void GenerateMinigames()
    {
        MinigameContainer.Minigames.Add(new Minigame(1));
        transform.GetChild(1).GetChild(2).GetChild(1).gameObject.GetComponent<Button>().ButtonAction = MinigameContainer.Minigames[0];
    }
}
