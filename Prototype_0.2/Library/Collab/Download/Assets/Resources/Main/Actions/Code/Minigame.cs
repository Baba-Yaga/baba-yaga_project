﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Minigame : Action
{
    public Minigame() { }

    public Minigame(int SceneIndex)
    {
        this.SceneIndex = SceneIndex;
    }

    public override void DoAction()
    {
        SceneManager.LoadScene(SceneIndex);

        //do close book animation
        //SceneManager.LoadSceneAsync(SceneIndex);
    }
}
