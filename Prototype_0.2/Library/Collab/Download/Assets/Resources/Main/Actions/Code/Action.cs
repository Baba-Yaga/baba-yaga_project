﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Action
{
    public string Text { get; set; }
    public string NextPageText { get; set; }
    public int SceneIndex { get; set; }

    public virtual void DoAction()
    {
        //ceva actiune
    }

    public virtual void LoadLevel(int sceneIndex)
    {
        GameObject go = GameObject.FindGameObjectWithTag("MaImpusc");
        go.GetComponent<SceneLoader>().LoadLevel(sceneIndex);
    }

    public virtual void EndOfAction()
    {
        //play flip page animation
        PageGenerator.GenerateLeftPage();
        PageGenerator.GenerateRightPage();
    }
}
