﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject Book;

    public void Start()
    {
        Book = GameObject.FindGameObjectWithTag("MaImpusc");
        Book.SetActive(false);
    }

    public void Win()
    {
        OutcomeContainer_SO so = Resources.Load<OutcomeContainer_SO>("Main/OutcomeContainer");
        so.Win = true;
        EndAndSwitchScene();
    }

    public void Lose()
    {
        OutcomeContainer_SO so = Resources.Load<OutcomeContainer_SO>("Main/OutcomeContainer");
        so.Win = false;
        EndAndSwitchScene();
    }

    public void EndAndSwitchScene()
    {
        Book.SetActive(true); staticadegeaba.ok = true;
        this.GetComponent<SceneLoader>().LoadLevel(0);
    }
}
