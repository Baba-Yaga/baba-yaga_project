﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClick : MonoBehaviour
{
    private Transform transform;
    private Vector3 targetPos;
    private float speed = 3f;
    // Start is called before the first frame update
    void Start()
    {
        transform = GetComponent<Transform>();
    }

    public void Move()
    {
        print("I am in");
        targetPos = transform.position;
        targetPos.y -= 1f;
        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
    }
}
