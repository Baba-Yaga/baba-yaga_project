﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bolMovement : MonoBehaviour
{
    private float move;
    private Vector3 movement;
    private float speed = 5f;
    public Text text;
    private int score = 0;

    // Start is called before the first frame update
    void Start()
    {
        text.text = "0";
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "enemy")
        {
            CanvasManager.Remove_Lifes();
        }
        else if(col.gameObject.tag == "food")
        {
            score++;
            text.text = score.ToString();
            if(score==30)
                GameObject.FindGameObjectWithTag("LVLMAN").GetComponent<LevelManager>().Win();
        }
        Destroy(col.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        move = Input.GetAxisRaw("Horizontal");
        movement = new Vector3(move, 0f, 0f);
        transform.position += movement / speed;
    }
}
