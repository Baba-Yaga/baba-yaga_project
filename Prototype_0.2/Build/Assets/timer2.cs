﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer2 : MonoBehaviour
{
    public float time;
    float minutes;
    float seconds;
    private Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (time > 0f)
            time -= Time.deltaTime;
        else if (time < 0f)
        {
            time = 0f;
            GameObject.FindGameObjectWithTag("LVLMAN").GetComponent<LevelManager>().Win();
        }
        minutes = Mathf.Floor(time / 60);
        seconds = Mathf.Floor(time % 60);
        text.text = string.Format("{0:0}:{1:00}", minutes, seconds);

    }
}
