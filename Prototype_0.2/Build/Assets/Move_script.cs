﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move_script : MonoBehaviour
{
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(-10f, this.transform.position.y, 0f), 5f * Time.deltaTime);
        if (transform.position.x == -10f)
            Destroy(this.gameObject);
    }
}
