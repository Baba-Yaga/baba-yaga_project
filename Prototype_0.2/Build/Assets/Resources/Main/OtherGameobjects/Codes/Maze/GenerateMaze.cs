﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateMaze : MonoBehaviour
{

    private GameObject[] spritePrefab_outer = new GameObject[4];
    private GameObject spritePrefab;
    private GameObject spritePrefab_start;
    private GameObject spritePrefab_finish;
    public GameObject tilemap;

    private void GenerateWalls()
    {
        spritePrefab_outer[1] = Resources.Load("Main/OtherGameobjects/Prefabs/Maze/MazeOuterWall_1") as GameObject;
        spritePrefab_outer[2] = Resources.Load("Main/OtherGameobjects/Prefabs/Maze/MazeOuterWall_2") as GameObject;
        spritePrefab_outer[3] = Resources.Load("Main/OtherGameobjects/Prefabs/Maze/MazeOuterWall_3") as GameObject;
        spritePrefab = Resources.Load("Main/OtherGameobjects/Prefabs/Maze/InnerWall") as GameObject;
        spritePrefab_start = Resources.Load("Main/OtherGameobjects/Prefabs/Maze/OuterWall_In") as GameObject;
        spritePrefab_finish = Resources.Load("Main/OtherGameobjects/Prefabs/Maze/OuterWall_Out") as GameObject;

        Instantiate(spritePrefab_outer[Random.Range(1,4)], new Vector3(1.5f, 0.5f, 0f), Quaternion.identity, tilemap.transform);
        Instantiate(spritePrefab_finish, new Vector3(19.5f, 0.5f, 0f), Quaternion.identity, tilemap.transform);
        Instantiate(spritePrefab_start, new Vector3(1.5f, 20.5f, 0f), Quaternion.identity, tilemap.transform);
        Instantiate(spritePrefab_outer[Random.Range(1, 4)], new Vector3(19.5f, 20.5f, 0f), Quaternion.identity, tilemap.transform);
        for (float i = 2.5f; i <= 18.5f; i += 1f)
        {
            Instantiate(spritePrefab_outer[Random.Range(1, 4)], new Vector3(i, 0.5f, 0f), Quaternion.identity, tilemap.transform);
            Instantiate(spritePrefab_outer[Random.Range(1, 4)], new Vector3(i, 20.5f, 0f), Quaternion.identity, tilemap.transform);
        }
        for (float i = 0.5f; i <= 20.5f; i += 1f)
        {
            Instantiate(spritePrefab_outer[Random.Range(1, 4)], new Vector3(0.5f, i, 0f), Quaternion.identity, tilemap.transform);
            Instantiate(spritePrefab_outer[Random.Range(1, 4)], new Vector3(20.5f, i, 0f), Quaternion.identity, tilemap.transform);
        }
    }

    int[] unde = new int[4];

    private void Linie_Generator(int linie,int start,int finish,int poz,float rotation)
    {
        if (unde[poz] == 0)
        {
           /// print(poz + ": " + "Linie:" + linie + "Start: " + start + " " + "Finish: " + finish);
            int lipsa = Random.Range(start, finish);
            float lipsa_float = lipsa - 0.5f;
           /// print(lipsa + " " + lipsa_float);
            lipsa = Random.Range(start, finish);
            lipsa_float = lipsa - 0.5f;
            for (float i = start - 0.5f; i <= finish - 0.5f; i += 1f)
            {
                if (i!=lipsa_float)
                    Instantiate(spritePrefab, new Vector3(i, linie, 0), Quaternion.Euler(0f, 0f, rotation), tilemap.transform);
            }
        }
        else
        {
            for (float i = start - 0.5f; i <= finish - 0.5f; i += 1f)
            {
                Instantiate(spritePrefab, new Vector3(i, linie, 0), Quaternion.Euler(0f, 0f, rotation), tilemap.transform);
            }
        }
    }

    private void Coloana_Generator(int coloana,int start, int finish, int poz, float rotation)
    {
        if (unde[poz] == 0)
        {
            ///print(poz + ": " + "Coloana:" + coloana + "Start: " + start + " " + "Finish: "+finish);
            int lipsa = Random.Range(start, finish);
            float lipsa_float = lipsa - 0.5f;
           /// print(lipsa + " " + lipsa_float);
            for (float i = start - 0.5f; i <= finish - 0.5f; i += 1f)
            {
                if (i != lipsa_float)
                    Instantiate(spritePrefab, new Vector3(coloana, i, 0), Quaternion.Euler(0f, 0f, rotation), tilemap.transform);
            }
        }
        else
        {
            for (float i = start - 0.5f; i <= finish - 0.5f; i += 1f)
            {
                Instantiate(spritePrefab, new Vector3(coloana, i, 0), Quaternion.Euler(0f, 0f, rotation), tilemap.transform);
            }
        }
    }

    private void Generate_Recursive(int coloana_start, int coloana_finish, int linie_start, int linie_finish)
    {
       /// print("De la coloana" + " " + coloana_start + " " + "Pana la coloana" + " " + coloana_finish + " " + "De la linia" + " " + linie_start + " " + "Pana la linia" + " " + linie_finish);
        if (coloana_finish - coloana_start == -1 || linie_finish - linie_start == -1)
            return;
        unde[Random.Range(0, 3)] = 1;
       /// print(unde[0] + " " + unde[1] + " " + unde[2] + " " + unde[3]);
        int linie = Random.Range(linie_start, linie_finish);
        int coloana = Random.Range(coloana_start, coloana_finish);
        Linie_Generator(linie,coloana_start, coloana, 0, 90f);
        Linie_Generator(linie, coloana + 1, coloana_finish + 1, 1, 90f);
        Coloana_Generator(coloana,linie_start, linie, 2, 0f);
        Coloana_Generator(coloana,linie + 1, linie_finish + 1, 3, 0f);

        for (int i = 0; i < 4; i++)
            unde[i] = 0;
        Generate_Recursive(coloana_start, coloana - 1, linie + 1, linie_finish); /// camera sus stanga
        Generate_Recursive(coloana + 1, coloana_finish, linie + 1, linie_finish); /// camera sus dreapta
        Generate_Recursive(coloana_start, coloana - 1, linie_start, linie -1); /// camera jos stanga
        Generate_Recursive(coloana + 1, coloana_finish, linie_start, linie -1); /// camera jos dreapta
        
        
        
    }

    // Start is called before the first frame update
    void Start()
    {
        GenerateWalls();
        for (int i = 0; i < 4; i++)
            unde[i] = 0;
        Generate_Recursive(2,19,2,19);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
