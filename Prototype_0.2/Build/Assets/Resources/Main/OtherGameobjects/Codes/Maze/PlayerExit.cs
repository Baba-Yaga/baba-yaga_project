﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerExit : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D col)
    {
        GameObject.FindGameObjectWithTag("LVLMAN").GetComponent<LevelManager>().Win();
    }
}
