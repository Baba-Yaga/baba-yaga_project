﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{
    private GameObject monster_prefab;
    private int seconds;
    private float timer;
    private float speed = 3f;
    private GameObject gameobject;
    private Vector3 targetPos;
    private GameObject[,] monsters = new GameObject[3, 3];


    // Start is called before the first frame update
    void Start()
    {
        monster_prefab = Resources.Load("Main/OtherGameobjects/Prefabs/Whackamole/MAHDUUUUUDE") as GameObject;
        seconds = Random.Range(2, 5);
        //print("Seconds: " + seconds);
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                monsters[i, j] = Instantiate(monster_prefab, new Vector3(j * 4f, (2 * (2 - i)), 0f), Quaternion.identity, this.transform);
            }
        }
    }


    int line, col;
    public static int ok = 0;
    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > 5f)
            timer -= 5f;
        if (ok == 0)
        {
            ok = 1;
            line = Random.Range(0, 3);
            col = Random.Range(0, 3);
            //print(line + " " + col);
            targetPos = new Vector3(col * 4f, (2-line)*2f + 1, 0f);
        }
        else if (ok==1)
        {
            monsters[line, col].transform.position = Vector3.MoveTowards(monsters[line, col].transform.position, targetPos, speed * Time.deltaTime);
        }


    }
}
