﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubmitButton : Button
{
    private void Start()
    {
        buttonPage = this.GetComponentInParent<ButtonPage>();
    }

    public override void DoAction()
    {
        print("submitted selected action");
        try
        {
            buttonPage.selectedAction.DoAction();
        } catch
        {
            print("no action selected!");
        }
    }
}
