﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Button : MonoBehaviour
{
    public Action ButtonAction;
    public bool Clicked = true;
    public ButtonPage buttonPage;

    private void Start()
    {
        buttonPage = this.GetComponentInParent<ButtonPage>();
    }

    public Button() { }

    public virtual void DoAction()
    {
        //print("action selected");
        if (buttonPage.selectedAction == null)
        {
            if (Clicked == false)
            {
                buttonPage.selectedAction = ButtonAction;
                Clicked = true;
            }
            else
            {
                Clicked = false;
            }
        }
        else
        {
            if (Clicked == false)
            {
                buttonPage.selectedAction = ButtonAction;
                Clicked = true;
            }
            else
            {
                buttonPage.selectedAction = null;
                Clicked = false;
            }
        }
        print(Clicked + ButtonAction.Text);
    }
}
