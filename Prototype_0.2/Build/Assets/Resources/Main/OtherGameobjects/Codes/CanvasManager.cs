﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static int lifes = 0;
    public static Image[] images = new Image[4];
    

    public static void Add_Lifes(int x, Image image , Canvas parent)
    {
        lifes++;
        images[x] = Instantiate(image, parent.transform);
        images[x].gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(x * 40f, -30f, 0f);
    }

    public static void Remove_Lifes()
    {
        Destroy(images[lifes--]);
        if(lifes == 0)
            GameObject.FindGameObjectWithTag("LVLMAN").GetComponent<LevelManager>().Lose();
    }
}
