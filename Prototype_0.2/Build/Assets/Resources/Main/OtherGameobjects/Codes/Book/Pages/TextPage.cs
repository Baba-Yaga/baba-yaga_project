﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TextPage : MonoBehaviour
{
    public GameObject Paragraph;
    private string path = "Assets/Resources/Main/Databases/Page1.txt";

    private void Start()
    {
        //CreateText(path);
    }

    public void CreateText(string path)
    {
        string x = File.ReadAllText(path);
        Paragraph = ParagraphMaker.CreateParagraph(x);
    }
}
