﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyhole_Script : MonoBehaviour
{
    private Transform transform_keyhole;
    public Transform transform_lockpick;
    public GameObject lockpick;
    public Transform transform_reference;
    private Quaternion rotation;
    private float angle; // Unghiul la care ajunge
    private Quaternion rotation_move;
    private float angle_move; // Unghiul in care trebuie sa fie lockpick.ul
    private float angle_current; // Unghiul in care este lockpickul
    private int ok = 1;
    // Start is called before the first frame update
    void Start()
    {
        transform_keyhole = GetComponent<Transform>();
        angle_move = Random.Range(-90, 90);
        print(angle_move);
    }

    // Update is called once per frame
    void Update()
    {
        angle_current = transform_lockpick.rotation.eulerAngles.z;
        angle_current = (angle_current > 180) ? angle_current - 360 : angle_current;
        transform_lockpick.position = new Vector3(transform_keyhole.position.x + transform_reference.position.x, transform_keyhole.position.y + transform_reference.position.y, transform_keyhole.position.z + transform_reference.position.z);
        if ((angle_current > angle_move - 8f && angle_current < angle_move + 7f))
        {
            int diff_angle = Mathf.Abs((int)angle_current - (int)angle_move);
            if (diff_angle > 3)
                angle = ((100f - (20f * (diff_angle - 3))) / 100f) * 90f;
            else
                angle = 90;
            //print(((100f - (20f * (diff_angle - 3))) / 100f) * 90f);
            angle *= -1;
            if (Input.GetKey("d"))
            {
                lockpick.GetComponent<Lockpick_Script>().enabled = false;
                rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 3f * Time.deltaTime);
            }
            else
            {
                lockpick.GetComponent<Lockpick_Script>().enabled = true;
                angle = 0;
                rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 3f * Time.deltaTime);
            }
        }
        else
        {
            lockpick.GetComponent<Lockpick_Script>().enabled = true;
            angle = 0;
            rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 3f * Time.deltaTime);
        }

        if (transform.rotation.eulerAngles.z >= 270f && transform.rotation.eulerAngles.z <= 272f && ok == 1)
        {
            ok = 0;
            GameObject.FindGameObjectWithTag("LVLMAN").GetComponent<LevelManager>().Win();
        }
    }
}
