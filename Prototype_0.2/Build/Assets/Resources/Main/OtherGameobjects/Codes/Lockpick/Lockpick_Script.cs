﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lockpick_Script : MonoBehaviour
{

    public Transform transform_Graphics;
    private Vector2 look;
    private float angle;
    private Quaternion rotation;
    private float speed = 5f;
    private Vector3 MousePos;
    
    void Update()
    {
        MousePos = Input.mousePosition;
        look = Camera.main.ScreenToWorldPoint(MousePos) - transform_Graphics.position;
        angle = Mathf.Atan2(look.y, look.x) * Mathf.Rad2Deg;
        if (look.y >= 0f)
        {
            rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            rotation *= Quaternion.Euler(0f, 0f, -90f);
        }
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speed * Time.deltaTime);

    }
}
