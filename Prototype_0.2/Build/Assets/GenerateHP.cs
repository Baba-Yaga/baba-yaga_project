﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateHP : MonoBehaviour
{
    public Image hearth_image;
    public int nr_hp;
    public Canvas canvas;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i <= nr_hp; i++)
            CanvasManager.Add_Lifes(i, hearth_image, canvas);
    }
}
