﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWalls : MonoBehaviour
{
    private GameObject wall1;
    private GameObject wall2;
    private GameObject wall3;
    private GameObject wall4;
    private float horSize;
    private float timer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        wall1 = Resources.Load("Main/OtherGameobjects/Prefabs/Run/Wall1") as GameObject;
        wall2 = Resources.Load("Main/OtherGameobjects/Prefabs/Run/Wall2") as GameObject;
        wall3 = Resources.Load("Main/OtherGameobjects/Prefabs/Run/Wall3") as GameObject;
        wall4 = Resources.Load("Main/OtherGameobjects/Prefabs/Run/Wall4") as GameObject;
        horSize = Camera.main.orthographicSize * Screen.width / Screen.height;
        horSize = Mathf.Floor(horSize);
        timer = Random.Range(2f, 3f);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        /// print(timer);
        if (timer < 0.0f)
        {
            float rand = Random.value;

            if (rand <= 0.25f)
            {
                Instantiate(wall1, new Vector3(horSize + 1f + Camera.main.transform.position.x, -3f, 0f), Quaternion.identity);
            }
            else if (rand <0.5)
            {
                Instantiate(wall2, new Vector3(horSize + 1f + Camera.main.transform.position.x, -2.5f, 0f), Quaternion.identity);
            }
            else if (rand < 0.75)
            {
                Instantiate(wall3, new Vector3(horSize + 1f + Camera.main.transform.position.x, -2f, 0f), Quaternion.identity);
            }
            else
            {
                Instantiate(wall4, new Vector3(horSize + 1f + Camera.main.transform.position.x, -1.5f, 0f), Quaternion.Euler(0, 0, 90));
            }
            timer = Random.Range(2f, 3f);
        }
    }
}
