﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;

public static class ParagraphMaker
{
    public static string path = "Main/OtherGameobjects/Prefabs/Paragraph/Paragraph";

    public static GameObject CreateParagraph(string x)
    {
        GameObject Paragraph = Object.Instantiate(Resources.Load(path) as GameObject) as GameObject;
        Paragraph.transform.position = new Vector3(-2.5f, 0f, 0f);
        
        TextMeshPro textMesh = Paragraph.GetComponent<TextMeshPro>();
        textMesh.text = x;
        return Paragraph;
    }
}
