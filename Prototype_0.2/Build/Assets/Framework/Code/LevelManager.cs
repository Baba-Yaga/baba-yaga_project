﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject Book;

    public void Start()
    {
        Book = GameObject.FindGameObjectWithTag("MaImpusc");
        Book.SetActive(false);
    }

    public void Win()
    {
        OutcomeContainer.Win = true;
        EndAndSwitchScene();
    }

    public void Lose()
    {
        OutcomeContainer.Win = false;
        EndAndSwitchScene();
    }

    public void EndAndSwitchScene()
    {
        //Book.name = "BOOK";
        Book.SetActive(true); staticadegeaba.ok = true;
        this.GetComponent<SceneLoader>().LoadLevel(0);
    }
}
