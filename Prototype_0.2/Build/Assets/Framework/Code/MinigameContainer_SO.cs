﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/MinigameContainer")]
public class MinigameContainer_SO : ScriptableObject
{
    public List<Action> Actions = new List<Action>();
    public List<Minigame> Minigames = new List<Minigame>();
}
